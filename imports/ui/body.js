import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

import { Thoughts } from '../api/thoughts.js';
import removeView from '../api/removeView.js';

import './components/thought/thought.js';
import './components/list/list.js';
import './components/home/home.js';
import './components/new/new.js';
import './body.html';


  //body template code
  Template.body.events({
    "click #header": function(event, template){
      event.preventDefault();
      if(Session.get('toggleCreate')==undefined){
          Session.set('toggleCreate',true);
      }

      if(Session.equals('toggleCreate',true)){
        var parentNode = $('.new-item').get(0);
        Blaze.render(Template.new, parentNode);
        Session.set("toggleCreate", false);
      }else {
        $('.new-item').removeClass('fadeInDown').addClass('fadeOutUp');
        Session.set('toggleCreate',true);
        Meteor.setTimeout(function(){
          removeView('.new-title');
        }, 900);
      }

    }

  });
