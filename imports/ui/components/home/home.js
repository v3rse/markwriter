import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';

import { Thoughts } from '../../../api/thoughts.js';
import removeView from '../../../api/removeView.js';

import './home.html'

//home template code
Template.home.helpers({
  thoughts:function () {
    return Thoughts.find();
  }
});

Template.home.onRendered(function () {
  $('.list-area').addClass('animated fadeIn');
})

Template.home.onCreated(function bodyOnCreated() {
  Meteor.subscribe("thoughtData");
})
