import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
import { parseMarkdown } from 'meteor/themeteorchef:commonmark';
import { moment } from 'meteor/momentjs:moment';
//couldn't import Clipboard. 'not constructor error' so using as is

import { Thoughts } from '../../../api/thoughts.js';
import removeView from '../../../api/removeView.js';

import './list.html';

//list template code
Template.list.helpers({
  formatDate: function(date){
    return moment(date).format('YYYY MMM D h:mm a')
  },
  parseMarkdown: function(stringHTml){
    return parseMarkdown(stringHTml);
  }
});

Template.list.events({
  "click  .title": function(event, template){
    event.preventDefault();
    $('.list-area').addClass('animated fadeOut');
    removeView('.list-area');
    var parentNode = $('.container').get(0);
    Blaze.renderWithData(Template.thought, this, parentNode);
  },
  "click .delete" : function (event, template) {
    event.preventDefault();
    var deleteButton = template.$('.delete');
    deleteButton.replaceWith("<div class='sure animated fadeIn'>Sure? <a href='#' class='button yes'>Yes</a> <a href='#' class='button no'>No</a></div>");
    this.deleteButton = deleteButton;
  },

  "click .yes": function (e) {
    e.preventDefault();
    Thoughts.remove(this._id);
  },

  "click .no": function (e) {
    e.preventDefault();
    $('.sure').replaceWith(this.deleteButton.addClass('animated fadeIn'));
  }
});

Template.list.onRendered(function () {
  $('.list-item').addClass('animated fadeIn');
  var clipboard = new Clipboard('.btn-copy-link');
})
