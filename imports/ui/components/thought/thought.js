//thought template code
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
import { MediumEditor } from 'meteor/mediumeditor:mediumeditor';
// import { toMarkdown } from 'meteor/brwn:to-markdown';
import { parseMarkdown } from 'meteor/themeteorchef:commonmark';

import { Thoughts } from '../../../api/thoughts.js';
import removeView  from '../../../api/removeView.js';

import './thought.html';

Template.thought.helpers({
  parseMarkdown: function(stringHTml){
    return parseMarkdown(stringHTml);
  }
});

Template.thought.events({
  "click #publish": function (event,template) {
    event.preventDefault();
    //save thought
      //save title
      var title = template.$('.thought-title p p').text() || template.$('.thought-title p').text() ;
      //save body
      var body = template.$('.thought-body').html();
      //save date edited
      var dateEdited = new Date();
      //save markdown
      //TODO: add title
      var body =  toMarkdown(body,{gfm:true});

    var editedThought = {
      title:title,
      body:body,
      dateEdited: dateEdited
    };

    //perform update
    Thoughts.update(this._id, {$set:editedThought},function (err,data) {
      if(err)
        console.error("Failed to save thought");

      $('.thought-item').addClass('animated fadeOut');
      removeView('.thought-item');
      var parentNode = $('.container').get(0);
      Blaze.render(Template.home, parentNode);
    });
  }
});

Template.thought.onRendered(function () {
  $('.thought-item').addClass('animated fadeIn');
  var titleEditor = new MediumEditor(this.$('.thought-title .editable').get(0));
  var bodyEditor = new MediumEditor(this.$('.thought-body.editable').get(0));
});

Template.thought.onCreated(function () {
})
