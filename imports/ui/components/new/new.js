import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
// import { toMarkdown } from 'meteor/brwn:to-markdown';

import { Thoughts } from '../../../api/thoughts.js';
import removeView from '../../../api/removeView.js';

import './new.html';

//new template code
Template.new.onRendered(function () {
  $('.new-item').removeClass('fadeOutUp').addClass('animated fadeInDown');
  var titleEditor = new MediumEditor($('.new-title .editable').get(0));
  var bodyEditor = new MediumEditor($('.new-body.editable').get(0));
});

Template.new.events({

      "click #create":function (e) {
        e.preventDefault();
        //save thought
          //save title
          var title = $('.new-title p p').text() || $('.new-title p').text() ;
          //save body
          var body = $('.new-body').html();
          //save date created
          var dateCreated = new Date();
          //save markdown
          //TODO: add title
          var body =  toMarkdown(body,{gfm:true});

        var createdThought = {
          title:title,
          body:body,
          dateCreated: dateCreated
        };

        //perform update
        Thoughts.insert(createdThought,function (err,data) {
          if(err)
            console.error("Failed to save thought");

            $('.new-item').removeClass('fadeInDown').addClass('fadeOutUp');
            Meteor.setTimeout(function(){
              Session.set('toggleCreate',true);
              removeView('.new-title');
            }, 900);
        });
      }
});
