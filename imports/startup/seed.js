import { Meteor } from 'meteor/meteor';
import { Thoughts } from '../api/thoughts.js';

Meteor.startup(function () {
  var sample_thought = {
      title:"The HEISENBERG",
      body:"The game has changed. The word is out. And you... are a killer. Apparently it's all over town. Somebody crossed you, you got angry, you crushed their skull with an ATM machine. Who cares! Just as long as it's our competitors who believe it and not the police.\nDon't you see how great this is? You, you are a... Jesse look at me. You... are a blowfish. A blowfish! Think about it. Small in stature, not swift, not cunning. Easy prey for predators but the blowfish has a secret weapon doesn't he. Doesn't he? What does the blowfish do, Jesse. What does the blowfish do? The blowfish puffs up, okay?\nThe blowfish puffs himself up four, five times larger than normal and why? Why does he do that? So that it makes him intimidating, that's why. Intimidating! So that the other, scarier fish are scared off. And that's you! You are a blowfish. You see it's just all an illusion. You see it's... it's nothing but air. Now... who messes with the blowfish, Jesse? You're damn right. You are a blowfish. Say it again. Say it like you mean it. You're a BLOWFISH!\nMy partner was about to get himself shot. I intervened. He was angry because those two dealers of yours had just murdered an eleven year-old boy. Then again, maybe he thought it was you who gave the order.",
      dateCreated:new Date()
    };

  if(Thoughts.find().count() == 0){
    Thoughts.insert(sample_thought);
  }
});
