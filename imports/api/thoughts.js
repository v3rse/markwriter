import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

//Collections
export const Thoughts = new Mongo.Collection("thoughts");


if(Meteor.isServer){
  Meteor.publish("thoughtData", function(){
    return Thoughts.find();
  });

}
