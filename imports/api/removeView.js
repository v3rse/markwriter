import { Blaze } from 'meteor/blaze';

export default function(selector) {
  var viewArea = $(selector);
  var view =  Blaze.getView(viewArea.get(0));
  Blaze.remove(view);
}
